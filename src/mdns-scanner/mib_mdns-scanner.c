/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
//#include <amxp/amxp_expression.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_expression.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <arpa/inet.h>
#include <gmap/gmap.h>

/**********************************************************
* Macro definitions
**********************************************************/
#define ME "gmap_mibs_mdns_scanner"

/**********************************************************
* Type definitions
**********************************************************/


/**********************************************************
* Variable declarations
**********************************************************/

amxb_bus_ctx_t* gmap_bus = NULL;

/**********************************************************
* Function Prototypes
**********************************************************/
amxd_status_t _setMDNSRecord(amxd_object_t* object,
                             amxd_function_t* func,
                             amxc_var_t* args,
                             amxc_var_t* ret);

/**********************************************************
* Functions
**********************************************************/

amxd_status_t _setMDNSRecord(amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxp_expr_status_t expr_status = amxp_expr_status_unknown_error;

    int index = 0;

    const char* device = amxd_object_get_value(cstring_t, object, "Alias", NULL);
    const char* alias = GET_CHAR(args, "alias");
    const char* type = GET_CHAR(args, "type");
    const char* entry = GET_CHAR(args, "entry");
    const char* value = GET_CHAR(args, "value");

    amxp_expr_t expr;
    amxd_object_t* mdnsrecord_template = NULL;
    amxd_object_t* record_obj = NULL;
    amxc_var_t values;
    amxc_string_t record_path;
    char* record_path_string = NULL;

    amxc_string_init(&record_path, 0);

    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_set(bool, ret, false);
    expr_status = amxp_expr_buildf(&expr, "Alias == '%s'", alias);
    when_failed_trace(expr_status, exit, ERROR, "Build expression for Alias %s failed: %d", alias, expr_status);

    /* Check if the arguments are correct */
    status = amxd_status_missing_key;
    when_str_empty(device, exit);
    when_str_empty(alias, exit);
    when_str_empty(type, exit);
    when_str_empty(entry, exit);
    when_str_empty(value, exit);

    amxc_var_add_key(cstring_t, &values, "Alias", alias);
    amxc_var_add_key(cstring_t, &values, "Type", type);
    amxc_var_add_key(cstring_t, &values, "Entry", entry);
    amxc_var_add_key(cstring_t, &values, "Value", value);

    mdnsrecord_template = amxd_object_get_child(object, "mDNSRecord");
    status = amxd_status_object_not_found;
    when_null(mdnsrecord_template, exit);

    /* Check if the instance already exists using the source value*/
    record_obj = amxd_object_find_instance(mdnsrecord_template, &expr);
    if(record_obj == NULL) {
        index = gmap_add_instance(device, "mDNSRecord", &values);
        if(index != 0) {
            status = amxd_status_ok;
        }
    } else {
        amxc_string_appendf(&record_path, "mDNSRecord.%s", alias);
        record_path_string = amxc_string_take_buffer(&record_path);
        status = gmap_set_subobject_instance(device, record_path_string, &values);
    }

    if(status == amxd_status_ok) {
        amxc_var_set(bool, ret, true);
    }

exit:
    amxp_expr_clean(&expr);
    amxc_var_clean(&values);
    amxc_string_clean(&record_path);
    free(record_path_string);
    free((char*) device);
    return status;
}
