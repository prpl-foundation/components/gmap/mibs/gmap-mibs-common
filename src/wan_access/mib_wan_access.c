/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>
#include <debug/sahtrace.h>

#include <gmap/gmap.h>
#include <gmap/gmap_device.h>
#include <gmap/gmap_config.h>

/**********************************************************
* Macro definitions
**********************************************************/

#define MIB_WANACCESS_FIELD_WANACCESS "WANAccess"
#define MIB_WANACCESS_FIELD_BLOCKED_REASONS "BlockedReasons"
#define MIB_WANACCESS_FIELD_BLOCKED_TAG "wan-blocked"

#define ME "mibs_wanaccess"

/**********************************************************
* Type definitions
**********************************************************/

typedef struct _mib_wan_access {
    amxb_bus_ctx_t* bus_ctx;
    amxd_dm_t* gmap_dm;
} mib_access_t;

/**********************************************************
* Variable declarations
**********************************************************/

static mib_access_t mib_access;

/**********************************************************
* Function Prototypes
**********************************************************/

amxd_status_t _block(amxd_object_t* device,
                     amxd_function_t* func,
                     amxc_var_t* args,
                     amxc_var_t* ret);
amxd_status_t _unblock(amxd_object_t* device,
                       amxd_function_t* func,
                       amxc_var_t* args,
                       amxc_var_t* ret);

/**********************************************************
* Functions
**********************************************************/

static void local_gmap_client_init(UNUSED const amxc_var_t* const data,
                                   UNUSED void* const priv) {
    if(mib_access.bus_ctx == NULL) {
        mib_access.bus_ctx = amxb_be_who_has("Devices.Device");
        gmap_client_init(mib_access.bus_ctx);
    }
    gmap_config_load("global");
}

CONSTRUCTOR static void mib_start(void) {
    amxp_sigmngr_deferred_call(NULL, local_gmap_client_init, NULL, NULL);
}

static bool block_wanaccess(amxd_object_t* device) {
    const char* key = amxd_object_get_name(device, AMXD_OBJECT_NAMED);
    bool result = gmap_device_setTag(key,
                                     MIB_WANACCESS_FIELD_BLOCKED_TAG,
                                     "",
                                     gmap_traverse_this);

    return result;
}

static bool unblock_wanaccess(amxd_object_t* device) {
    const char* key = amxd_object_get_name(device, AMXD_OBJECT_NAMED);
    bool result = gmap_device_clearTag(key,
                                       MIB_WANACCESS_FIELD_BLOCKED_TAG,
                                       NULL,
                                       gmap_traverse_this);

    return result;
}

static bool string_in_string_list(amxc_var_t* haystack, char* needle) {
    bool present = false;

    amxc_var_for_each(value, haystack) {
        const char* str_val = GET_CHAR(value, NULL);
        if((str_val != NULL) && (strcmp(str_val, needle) == 0)) {
            present = true;
            break;
        }
    }

    return present;
}

static bool reason_ok(char* reason,
                      amxc_var_t* blocked_reasons) {
    bool reason_ok = false;
    amxc_var_t* available_reasons = gmap_config_get("global", MIB_WANACCESS_FIELD_BLOCKED_REASONS);

    when_null(available_reasons, exit);

    amxc_var_cast(available_reasons, AMXC_VAR_ID_SSV_STRING);
    amxc_var_cast(available_reasons, AMXC_VAR_ID_LIST);

    /* check that it is a valid block reason */
    if(string_in_string_list(available_reasons, reason)) {
        /* check if there are no block reasons present yet */
        if(!string_in_string_list(blocked_reasons, reason)) {
            reason_ok = true;
        }
    }

exit:
    amxc_var_delete(&available_reasons);
    return reason_ok;
}

static char* create_new_reason_list(amxc_var_t* blocked_reasons, char* reason) {
    char* new_blocked_reasons = NULL;

    amxc_var_for_each(value, blocked_reasons) {
        const char* str_val = GET_CHAR(value, NULL);
        if((str_val != NULL) && (strcmp(str_val, reason) == 0)) {
            amxc_var_delete(&value);
        }
    }

    amxc_var_cast(blocked_reasons, AMXC_VAR_ID_SSV_STRING);
    new_blocked_reasons = amxc_var_dyncast(cstring_t, blocked_reasons);

    return new_blocked_reasons;
}

amxd_status_t _block(amxd_object_t* object,
                     UNUSED amxd_function_t* func,
                     amxc_var_t* args,
                     UNUSED amxc_var_t* ret) {
    /* Block the device with the given reason */
    amxc_var_t blocked_list;
    amxc_var_init(&blocked_list);

    char* reason = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_object_t* device = amxd_object_get_parent(object);

    when_null(device, exit);

    status = amxd_object_get_param(object,
                                   MIB_WANACCESS_FIELD_BLOCKED_REASONS,
                                   &blocked_list);
    when_failed(status, exit);

    amxc_var_cast(&blocked_list, AMXC_VAR_ID_LIST);
    if(amxc_var_type_of(&blocked_list) != AMXC_VAR_ID_LIST) {
        amxc_var_set_type(&blocked_list, AMXC_VAR_ID_LIST);
    }

    reason = amxc_var_dyncast(cstring_t, GET_ARG(args, "reason"));

    /* check if it is a valid reason */
    if(reason_ok(reason, &blocked_list)) {
        char* new_blocked_reasons = NULL;
        /* add it to the blocked reason list */
        amxc_var_add(cstring_t, &blocked_list, reason);
        amxc_var_cast(&blocked_list, AMXC_VAR_ID_SSV_STRING);
        new_blocked_reasons = amxc_var_dyncast(cstring_t, &blocked_list);

        amxd_object_set_cstring_t(object,
                                  MIB_WANACCESS_FIELD_BLOCKED_REASONS,
                                  new_blocked_reasons);

        free(new_blocked_reasons);
        /* block the wan acces for the device */
        block_wanaccess(device);
    } else {
        status = amxd_status_invalid_function_argument;
    }

exit:
    free(reason);
    amxc_var_clean(&blocked_list);
    return status;
}

amxd_status_t _unblock(amxd_object_t* object,
                       UNUSED amxd_function_t* func,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    /* Unblock the device for the given reason, if no reason is given, unblock for all reasons */
    amxc_var_t blocked_list;
    amxc_var_init(&blocked_list);

    char* reason = NULL;
    char* new_blocked_reasons = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* device = amxd_object_get_parent(object);

    amxc_var_cast(&blocked_list, AMXC_VAR_ID_SSV_STRING);

    /* fetch current blocked reasons */
    status = amxd_object_get_param(object,
                                   MIB_WANACCESS_FIELD_BLOCKED_REASONS,
                                   &blocked_list);
    when_failed(status, exit);

    amxc_var_cast(&blocked_list, AMXC_VAR_ID_LIST);
    if(amxc_var_type_of(&blocked_list) != AMXC_VAR_ID_LIST) {
        amxc_var_set_type(&blocked_list, AMXC_VAR_ID_LIST);
    }

    reason = amxc_var_dyncast(cstring_t, GET_ARG(args, "reason"));

    /* If the reason is empty
     * clear all blocked reasons*/
    if((reason == NULL) || (*reason == 0)) {
        amxd_object_set_cstring_t(object, MIB_WANACCESS_FIELD_BLOCKED_REASONS, "");
        unblock_wanaccess(device);
    }
    /* If the reason is not empty
    * clear reason passed along*/
    else if(string_in_string_list(&blocked_list, reason)) {
        new_blocked_reasons = create_new_reason_list(&blocked_list, reason);
        amxd_object_set_cstring_t(object,
                                  MIB_WANACCESS_FIELD_BLOCKED_REASONS,
                                  new_blocked_reasons);
        if((new_blocked_reasons == NULL) || (*new_blocked_reasons == 0)) {
            unblock_wanaccess(device);
        }

        free(new_blocked_reasons);
    } else {
        status = amxd_status_invalid_function_argument;
    }

exit:
    /* free blocked_reasons */
    amxc_var_clean(&blocked_list);
    free(reason);
    return status;
}
