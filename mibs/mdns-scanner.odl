/* No expression needed */

import "mib_mdns-scanner.so";

/**
 * MIB is loaded on all devices which are detected to have at least one mDNS service by gmap-mod-mdns-scanner
 *
 * All devices matching expression: "mdns-scanner" are extended with this MIB
 *
 * See RFC 6763 regarding mDNS service discovery
 *
 * @version 1.0
 */
mib mdns-scanner {

    /**
     * Add or modify a mdns record.
     *
     * @param alias denotes the alias of a specific mdns record that has to be created or modified. This will be used to easily identify a specific mdns record.
     * @param type is the type of the mdns record
     * @param entry is the entry of the mdns record
     * @param value is the value of the new mdns record
     *
     * @error amxd_status_unknown_error
     *        amxd_status_missing_key
     *        amxd_status_object_not_found
     *        amxd_status_invalid_arg
     *
     * @version 1.0
     */
    bool setMDNSRecord(%mandatory string alias, %mandatory string type, %mandatory string entry, %mandatory string value);

    /**
     * List of detected mDNS records of the device.
     *
     * @version 1.0
     */
    %persistent object mDNSRecord[] {
        on action add-inst call verify_max_instances "Devices.Config.mDNSScanner.MaxMdnsRecords";

        %persistent %unique %key string Alias;

        /**
         * mDNS record type.
         *
         * @version 1.0
         */
        %persistent string Type;

        /**
         * mDNS record entry.
         *
         * @version 1.0
         */
        %persistent string Entry {
                on action validate call check_maximum_length 255;
        }

        /**
         * mDNS record value.
         *
         * @version 1.0
         */
        %persistent string Value {
            on action validate call check_maximum_length 255;
        }
    }
}
