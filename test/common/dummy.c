/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>

#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_function.h>
#include <amxb/amxb_be.h>

#include <amxo/amxo.h>
#include <amxc/amxc_macros.h>

#include "dummy.h"

static amxd_dm_t remote_dm;
static amxo_parser_t parser;
static amxp_signal_mngr_t* dummy_sigmngr = NULL;
static amxc_htable_t subscriptions;


void amxb_dummy_send_notification(const char* const sig_name,
                                  const amxc_var_t* const data,
                                  void* const priv) {
    amxc_var_t notification;
    amxc_htable_it_t* it = (amxc_htable_it_t*) priv;
    amxc_var_init(&notification);
    amxc_var_set_type(&notification, AMXC_VAR_ID_HTABLE);

    amxc_var_copy(&notification, data);
    amxc_var_add_key(cstring_t, &notification, "notification", sig_name);
    amxc_var_dump(&notification, STDOUT_FILENO);

    amxp_sigmngr_emit_signal(dummy_sigmngr, amxc_htable_it_get_key(it), &notification);

    amxc_var_clean(&notification);
}

static void amxb_dummy_free_subscription(UNUSED const char* key, amxc_htable_it_t* it) {
    amxp_slot_disconnect_with_priv(&remote_dm.sigmngr, amxb_dummy_send_notification, it);
    free(it);
}

static void* amxb_dummy_connect(UNUSED const char* host,
                                UNUSED const char* port,
                                UNUSED const char* path,
                                amxp_signal_mngr_t* sigmngr) {

    amxd_dm_init(&remote_dm);
    amxo_parser_init(&parser);
    amxc_htable_init(&subscriptions, 10);
    dummy_sigmngr = sigmngr;
    return &remote_dm;
}

static int amxb_dummy_disconnect(UNUSED void* ctx) {
    amxd_dm_clean(&remote_dm);
    amxo_parser_clean(&parser);
    dummy_sigmngr = NULL;
    return 0;
}

static int amxb_dummy_invoke(UNUSED void* const ctx,
                             amxb_invoke_t* invoke_ctx,
                             amxc_var_t* args,
                             amxb_request_t* request,
                             UNUSED int timeout) {

    amxc_var_t empty_args;
    amxc_var_t* return_value = NULL;
    amxc_var_init(&empty_args);
    amxc_var_set_type(&empty_args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(request->result, AMXC_VAR_ID_LIST);

    int rv = 0;

    if(args == NULL) {
        args = &empty_args;
    }

    return_value = amxc_var_add_new(request->result);
    amxd_object_t* obj = amxd_dm_findf(&remote_dm, invoke_ctx->object);
    rv = amxd_object_invoke_function(obj, invoke_ctx->method, args, return_value);

    amxc_var_clean(&empty_args);
    return rv;
}

static void amxb_dummy_free(UNUSED void* ctx) {
    amxo_parser_clean(&parser);
    amxd_dm_clean(&remote_dm);
    amxc_htable_clean(&subscriptions, amxb_dummy_free_subscription);
}

bool amxb_dummy_has(UNUSED void* const ctx,
                    const char* object) {
    amxd_object_t* obj = NULL;
    when_str_empty(object, exit);
    obj = amxd_dm_findf(&remote_dm, "%s", object);

exit:
    return obj != NULL;
}

static int amxb_dummy_register(UNUSED void* const ctx,
                               UNUSED amxd_dm_t* const dm) {
    return 0;
}

static int amxb_dummy_subscribe(UNUSED void* const ctx,
                                const char* object) {

    amxc_string_t expression;
    amxc_htable_it_t* it = NULL;

    amxc_string_init(&expression, 0);

    it = (amxc_htable_it_t*) calloc(1, sizeof(amxc_htable_it_t));
    amxc_htable_insert(&subscriptions, object, it);
    amxc_string_appendf(&expression, "path starts with \"%s.\"", object);
    amxp_slot_connect_filtered(&remote_dm.sigmngr,
                               ".*",
                               amxc_string_get(&expression, 0),
                               amxb_dummy_send_notification,
                               it);

    amxc_string_clean(&expression);
    return 0;
}

static int amxb_dummy_unsubscribe(UNUSED void* const ctx,
                                  const char* object) {
    amxc_htable_it_t* it = amxc_htable_get(&subscriptions, object);
    amxc_htable_it_clean(it, amxb_dummy_free_subscription);

    return 0;
}

static uint32_t amxb_dummy_capabilities(UNUSED void* const ctx) {
    return AMXB_BE_DISCOVER;
}

static amxb_be_funcs_t amxb_dummy_impl = {
    .connect = amxb_dummy_connect,
    .disconnect = amxb_dummy_disconnect,
    .get_fd = NULL,
    .read = NULL,
    .invoke = amxb_dummy_invoke,
    .async_invoke = NULL,
    .wait_request = NULL,
    .close_request = NULL,
    .free = amxb_dummy_free,
    .register_dm = amxb_dummy_register,
    .subscribe = amxb_dummy_subscribe,
    .unsubscribe = amxb_dummy_unsubscribe,
    .has = amxb_dummy_has,
    .capabilities = amxb_dummy_capabilities,
    .name = "dummy",
    .size = sizeof(amxb_be_funcs_t),
};

int test_register_dummy(void) {
    return amxb_be_register(&amxb_dummy_impl);
}

int test_unregister_dummy(void) {
    return amxb_be_unregister(&amxb_dummy_impl);
}

int test_load_dummy_remote(const char* odl) {
    amxd_object_t* root_obj = amxd_dm_get_root(&remote_dm);
    return amxo_parser_parse_file(&parser, odl, root_obj);
}

amxo_parser_t* test_get_parser(void) {
    return &parser;
}

amxd_dm_t* test_get_dm(void) {
    return &remote_dm;
}

void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

amxd_status_t _load(AMXB_UNUSED amxd_object_t* object,
                    AMXB_UNUSED amxd_function_t* func,
                    AMXB_UNUSED amxc_var_t* args,
                    amxc_var_t* ret) {

    amxc_var_set(bool, ret, true);

    return amxd_status_ok;
}

amxd_status_t _get(amxd_object_t* object,
                   AMXB_UNUSED amxd_function_t* func,
                   amxc_var_t* args,
                   amxc_var_t* ret) {
    const char* module = GET_CHAR(args, "module");
    const char* option = GET_CHAR(args, "option");

    amxd_object_t* child = amxd_object_get_child(object, module);
    amxd_object_get_param(child, option, ret);

    return amxd_status_ok;
}

amxd_status_t _setTag(amxd_object_t* object,
                      AMXB_UNUSED amxd_function_t* func,
                      amxc_var_t* args,
                      AMXB_UNUSED amxc_var_t* ret) {
    const char* tag = GET_CHAR(args, "tag");
    amxc_var_t tags;
    amxc_var_init(&tags);

    amxd_object_get_param(object, "Tags", &tags);

    amxc_var_cast(&tags, AMXC_VAR_ID_SSV_STRING);
    amxc_var_cast(&tags, AMXC_VAR_ID_LIST);

    amxc_var_for_each(value, &tags) {
        const char* str_val = GET_CHAR(value, NULL);
        if((str_val != NULL) && (strcmp(str_val, tag) == 0)) {
            goto exit;
        }
    }

    amxc_var_add(cstring_t, &tags, tag);
    amxc_var_cast(&tags, AMXC_VAR_ID_SSV_STRING);
    amxd_object_set_param(object, "Tags", &tags);

exit:
    amxc_var_clean(&tags);
    return amxd_status_ok;
}

amxd_status_t _clearTag(amxd_object_t* object,
                        AMXB_UNUSED amxd_function_t* func,
                        amxc_var_t* args,
                        AMXB_UNUSED amxc_var_t* ret) {
    const char* tag = GET_CHAR(args, "tag");
    amxc_var_t tags;
    amxc_var_init(&tags);

    amxd_object_get_param(object, "Tags", &tags);

    amxc_var_cast(&tags, AMXC_VAR_ID_SSV_STRING);
    amxc_var_cast(&tags, AMXC_VAR_ID_LIST);

    amxc_var_for_each(var, &tags) {
        const char* value = GET_CHAR(var, NULL);
        if((tag != NULL) && (strcmp(value, tag) == 0)) {
            amxc_var_delete(&var);
        }
    }

    amxc_var_cast(&tags, AMXC_VAR_ID_SSV_STRING);
    amxd_object_set_param(object, "Tags", &tags);

    amxc_var_clean(&tags);
    return amxd_status_ok;
}

amxd_status_t _setActive(amxd_object_t* object,
                         AMXB_UNUSED amxd_function_t* func,
                         amxc_var_t* args,
                         AMXB_UNUSED amxc_var_t* ret) {
    bool active = GET_BOOL(args, "active");
    const char* source = GET_CHAR(args, "string");
    uint32_t priority = GET_UINT32(args, "priority");

    amxd_object_t* actives = amxd_object_get_child(object, "Actives");
    amxd_object_t* child = amxd_object_get_instance(object, source, 0);

    if(child == NULL) {
        amxc_var_t values;
        amxc_var_init(&values);
        amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &values, "Source", source);
        amxc_var_add_key(uint32_t, &values, "Priority", priority);
        amxc_var_add_key(bool, &values, "Active", active);
        amxd_object_new_instance(&child, actives, source, 0, &values);
        amxc_var_clean(&values);
    } else {
        amxd_object_set_value(uint32_t, child, "Priority", priority);
        amxd_object_set_value(bool, child, "Active", active);
    }

    priority = UINT32_MAX;
    active = false;
    amxd_object_iterate(instance, it, actives) {
        amxd_object_t* child = amxc_container_of(it, amxd_object_t, it);
        uint32_t child_priority = amxd_object_get_value(uint32_t, child, "Priority", NULL);
        bool child_active = amxd_object_get_value(uint32_t, child, "Active", NULL);
        if(child_priority < priority) {
            active = child_active;
            priority = child_priority;
        } else if(child_priority == priority) {
            active = active && child_active;
        }
    }
    amxd_object_set_value(bool, object, "Active", active);

    return amxd_status_ok;
}