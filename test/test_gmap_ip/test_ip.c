/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_variant.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_expression.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <amxo/amxo_mibs.h>

#include <gmap/gmap.h>

#include "test_ip.h"
#include "mib_ip.h"
#include "../common/dummy.h"

uint32_t gmap_ip_family_string2id(const char* family);

/**********************************************************
* Macro definitions
**********************************************************/

#define MIB_IPv4 "IPv4Address"
#define MIB_IPv6 "IPv6Address"
#define DEV_IPv4_PATH "Devices.Device.temp.IPv4Address."
#define DEV_IPv6_PATH "Devices.Device.temp.IPv6Address."
#define DEV_PATH "Devices.Device.temp."
#define AF_INET 2
#define AF_INET6 10


/**********************************************************
* Variable declarations
**********************************************************/
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxd_object_t* dev_obj = NULL;
static amxd_object_t* dev_lan = NULL;

amxd_status_t _hasTag(amxd_object_t* object,
                      UNUSED amxd_function_t* func,
                      amxc_var_t* args,
                      amxc_var_t* ret) {
    bool present = true;
    amxc_string_t tag_string;
    amxc_var_t tag_list;
    const amxc_var_t* param = amxd_object_get_param_value(object, "Tags");
    const char* tags = amxc_var_constcast(cstring_t, param);

    amxc_string_init(&tag_string, 0);
    amxc_string_set(&tag_string, GET_CHAR(args, "tag"));

    amxc_var_init(&tag_list);
    amxc_string_ssv_to_var(&tag_string, &tag_list, NULL);

    amxc_var_for_each(var, &tag_list) {
        const char* needle = amxc_var_constcast(cstring_t, var);
        if(!strstr(tags, needle)) {
            present = false;
            break;
        }
    }

    amxc_var_set(bool, ret, present);
    amxc_var_clean(&tag_list);
    amxc_string_clean(&tag_string);

    return amxd_status_ok;
}

uint32_t gmap_ip_family_string2id(const char* family) {
    uint32_t id = 0;

    if((family != NULL) && (family[0] != '\0')) {
        if(strcasecmp(family, "ipv4") == 0) {
            id = AF_INET;
        } else if(strcasecmp(family, "ipv6") == 0) {
            id = AF_INET6;
        }
    }

    return id;
}

static void add_ip_instance(amxd_object_t* dev, char* address, char* source, char* status, char* family) {
    int retval = 0;
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxc_string_t path;
    amxd_object_t* template = amxd_object_get_child(dev, family);
    char* object_path = amxd_object_get_path(dev, AMXD_OBJECT_TERMINATE);

    assert_non_null(template);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "address", address);
    amxc_var_add_key(cstring_t, &args, "source", source);
    amxc_var_add_key(cstring_t, &args, "status", status);

    if(strcmp(family, MIB_IPv4) == 0) {
        amxc_var_add_key(bool, &args, "reserved", true);
    }

    amxc_string_setf(&path, "%s%s.", object_path, family);

    retval = amxb_call(bus_ctx, amxc_string_get(&path, 0), "addAddressInstance", &args, &ret, 5);
    assert_int_equal(retval, amxd_status_ok);
    handle_events();

    if(status == NULL) {
        status = "not reachable";
    }
    amxp_expr_buildf(&expr, "Address == '%s' && Status == '%s'", address, status);
    assert_non_null(amxd_object_find_instance(template, &expr));

    free(object_path);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&path);
    amxp_expr_clean(&expr);
}

static void add_ipv4_defaults() {
    expect_string_count(__wrap_gmap_device_setActive, key, "temp", 2);
    expect_value_count(__wrap_gmap_device_setActive, active, true, 2);
    expect_string_count(__wrap_gmap_device_setActive, source, "mib-ip", 2);
    expect_value_count(__wrap_gmap_device_setActive, priority, 50, 2);

    will_return(__wrap_gmap_device_setActive, 1);
    add_ip_instance(dev_obj, "192.168.1.5", "DHCP", "reachable", MIB_IPv4);

    add_ip_instance(dev_obj, "192.168.1.6", "DHCP", "not reachable", MIB_IPv4);

    will_return(__wrap_gmap_device_setActive, 1);
    add_ip_instance(dev_obj, "192.168.1.7", "Static", "reachable", MIB_IPv4);

    handle_events();
}

static void add_ipv6_defaults() {
    expect_string_count(__wrap_gmap_device_setActive, key, "temp", 2);
    expect_value_count(__wrap_gmap_device_setActive, active, true, 2);
    expect_string_count(__wrap_gmap_device_setActive, source, "mib-ip", 2);
    expect_value_count(__wrap_gmap_device_setActive, priority, 50, 2);

    will_return(__wrap_gmap_device_setActive, 1);
    add_ip_instance(dev_obj, "fe80::f606:8dff:fe11:a5e9", "DHCP", "reachable", MIB_IPv6);

    will_return(__wrap_gmap_device_setActive, 1);
    add_ip_instance(dev_obj, "fc00:1002:fc01:3000:2000::1001", "DHCP", "reachable", MIB_IPv6);

    add_ip_instance(dev_obj, "fc00:1002:fc01:3000:2000::1002", "Static", "not reachable", MIB_IPv6);

    handle_events();
}

static bool load_mib_module() {
    amxo_parser_t* parser = test_get_parser();
    amxd_dm_t* dm = test_get_dm();
    dev_obj = amxd_object_findf(&dm->object, "Devices.Device.temp.");
    dev_lan = amxd_object_findf(&dm->object, "Devices.Device.lan.");

    assert_int_equal(amxo_resolver_ftab_add(parser, "addAddressInstance", AMXO_FUNC(_addAddressInstance)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "deleteAddressInstance", AMXO_FUNC(_deleteAddressInstance)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "setAddressInstance", AMXO_FUNC(_setAddressInstance)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "getAddress", AMXO_FUNC(_getAddress)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ip_address_instance_removed", AMXO_FUNC(_ip_address_instance_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ip_address_instance_added", AMXO_FUNC(_ip_address_instance_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "ip_address_instance_changed", AMXO_FUNC(_ip_address_instance_changed)), 0);

    amxo_parser_scan_mib_dir(parser, "../../mibs");
    amxo_parser_load_mib(parser, dm, "ip");

    amxo_parser_apply_mib(parser, dev_obj, "ip");
    amxo_parser_apply_mib(parser, dev_lan, "ip");

    return true;
}

int test_ip_setup(AMXB_UNUSED void** state) {
    amxc_var_t* var = NULL;
    amxo_parser_t* parser = NULL;

    assert_int_equal(test_register_dummy(), 0);
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    gmap_client_init(bus_ctx);

    // parser and dm are created in dummy connect
    parser = test_get_parser();

    assert_int_equal(amxo_resolver_ftab_add(parser, "load", AMXO_FUNC(_load)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "get", AMXO_FUNC(_get)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "setTag", AMXO_FUNC(_setTag)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "clearTag", AMXO_FUNC(_clearTag)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "setActive", AMXO_FUNC(_setActive)), 0);

    gmap_client_set_mib_server_dm(test_get_dm());

    assert_int_equal(amxo_resolver_ftab_add(parser, "hasTag", AMXO_FUNC(_hasTag)), 0);

    // do not load import so files
    var = amxo_parser_claim_config(parser, "odl-import");
    amxc_var_set(bool, var, false);

    assert_int_equal(test_load_dummy_remote("../common/dummy_gmap.odl"), 0);

    load_mib_module();

    return amxd_status_ok;
}

int test_ip_teardown(AMXB_UNUSED void** state) {
    amxo_parser_remove_mibs(test_get_parser(), dev_obj, NULL);
    amxb_disconnect(bus_ctx);
    amxb_free(&bus_ctx);
    assert_int_equal(test_unregister_dummy(), 0);

    return amxd_status_ok;
}

void test_add_ipv4_address(AMXB_UNUSED void** state) {
    expect_string_count(__wrap_gmap_device_setActive, key, "temp", 1);
    expect_value_count(__wrap_gmap_device_setActive, active, false, 1);
    expect_string_count(__wrap_gmap_device_setActive, source, "mib-ip", 1);
    expect_value_count(__wrap_gmap_device_setActive, priority, 50, 1);
    will_return(__wrap_gmap_device_setActive, 0);

    add_ip_instance(dev_obj, "192.168.1.5", "DHCP", NULL, MIB_IPv4);
}

void test_add_ipv6_address(AMXB_UNUSED void** state) {
    expect_string_count(__wrap_gmap_device_setActive, key, "temp", 1);
    expect_value_count(__wrap_gmap_device_setActive, active, false, 1);
    expect_string_count(__wrap_gmap_device_setActive, source, "mib-ip", 1);
    expect_value_count(__wrap_gmap_device_setActive, priority, 50, 1);
    will_return(__wrap_gmap_device_setActive, 0);

    add_ip_instance(dev_obj, "fe80::f606:8dff:fe11:a5e8", "DHCP", NULL, MIB_IPv6);
}

void test_update_ipv4_address(AMXB_UNUSED void** state) {
    int status = 0;
    char* ip = "192.168.1.5";
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxc_string_t path;
    amxd_object_t* template = amxd_object_get_child(dev_obj, MIB_IPv4);

    assert_non_null(template);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    expect_string_count(__wrap_gmap_device_setActive, key, "temp", 1);
    expect_value_count(__wrap_gmap_device_setActive, active, false, 1);
    expect_string_count(__wrap_gmap_device_setActive, source, "mib-ip", 1);
    expect_value_count(__wrap_gmap_device_setActive, priority, 50, 1);
    will_return(__wrap_gmap_device_setActive, 0);

    add_ip_instance(dev_obj, "192.168.1.5", "DHCP", NULL, MIB_IPv4);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "address", ip);
    amxc_var_add_key(cstring_t, &args, "status", "reachable");

    expect_string_count(__wrap_gmap_device_setActive, key, "temp", 1);
    expect_value_count(__wrap_gmap_device_setActive, active, true, 1);
    expect_string_count(__wrap_gmap_device_setActive, source, "mib-ip", 1);
    expect_value_count(__wrap_gmap_device_setActive, priority, 50, 1);
    will_return(__wrap_gmap_device_setActive, 0);

    status = amxb_call(bus_ctx, DEV_IPv4_PATH, "setAddressInstance", &args, &ret, 5);
    assert_int_equal(status, amxd_status_ok);
    handle_events();

    amxp_expr_buildf(&expr, "Address == '%s' && Status == 'reachable'", ip);
    assert_non_null(amxd_object_find_instance(template, &expr));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxp_expr_clean(&expr);
}

void test_update_ipv6_address(AMXB_UNUSED void** state) {
    int status = 0;
    char* ip = "fe80::f606:8dff:fe11:a5e8";
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxc_string_t path;
    amxd_object_t* template = amxd_object_get_child(dev_obj, MIB_IPv6);

    assert_non_null(template);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    expect_string_count(__wrap_gmap_device_setActive, key, "temp", 1);
    expect_value_count(__wrap_gmap_device_setActive, active, false, 1);
    expect_string_count(__wrap_gmap_device_setActive, source, "mib-ip", 1);
    expect_value_count(__wrap_gmap_device_setActive, priority, 50, 1);
    will_return(__wrap_gmap_device_setActive, 0);

    add_ip_instance(dev_obj, "fe80::f606:8dff:fe11:a5e8", "DHCP", NULL, MIB_IPv6);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "address", ip);
    amxc_var_add_key(cstring_t, &args, "status", "reachable");

    expect_string_count(__wrap_gmap_device_setActive, key, "temp", 1);
    expect_value_count(__wrap_gmap_device_setActive, active, true, 1);
    expect_string_count(__wrap_gmap_device_setActive, source, "mib-ip", 1);
    expect_value_count(__wrap_gmap_device_setActive, priority, 50, 1);
    will_return(__wrap_gmap_device_setActive, 0);

    status = amxb_call(bus_ctx, DEV_IPv6_PATH, "setAddressInstance", &args, &ret, 5);
    assert_int_equal(status, amxd_status_ok);
    handle_events();

    amxp_expr_buildf(&expr, "Address == '%s' && Status == 'reachable'", ip);
    assert_non_null(amxd_object_find_instance(template, &expr));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxp_expr_clean(&expr);
}

void test_add_ipv4_address_wrong_status(AMXB_UNUSED void** state) {
    int status = 0;
    char* ip = "192.168.1.100";
    char* source = "DHCP";
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* template = amxd_object_get_child(dev_obj, MIB_IPv4);

    assert_non_null(template);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "address", ip);
    amxc_var_add_key(cstring_t, &args, "addressSource", source);
    amxc_var_add_key(cstring_t, &args, "status", "wrong status");
    amxc_var_add_key(bool, &args, "reserved", true);

    status = amxb_call(bus_ctx, DEV_IPv4_PATH, "addAddressInstance", &args, &ret, 5);
    assert_int_equal(status, amxd_status_invalid_function_argument);
    handle_events();

    amxp_expr_buildf(&expr, "Address == '%s' && Status == 'wrong status'", ip);
    assert_null(amxd_object_find_instance(template, &expr));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxp_expr_clean(&expr);
}

void test_add_ipv6_address_wrong_status(AMXB_UNUSED void** state) {
    int status = 0;
    char* ip = "fe80::8888:8888:8888:8888";
    char* source = "DHCP";
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* template = amxd_object_get_child(dev_obj, MIB_IPv6);

    assert_non_null(template);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "address", ip);
    amxc_var_add_key(cstring_t, &args, "addressSource", source);
    amxc_var_add_key(cstring_t, &args, "status", "wrong status");

    status = amxb_call(bus_ctx, DEV_IPv6_PATH, "addAddressInstance", &args, &ret, 5);
    assert_int_equal(status, amxd_status_invalid_function_argument);
    handle_events();

    amxp_expr_buildf(&expr, "Address == '%s' && Status == 'wrong status'", ip);
    assert_null(amxd_object_find_instance(template, &expr));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxp_expr_clean(&expr);
}

void test_getAddress_ipv4(AMXB_UNUSED void** state) {
    int status = 0;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* instance = NULL;
    amxc_var_t* address = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    // add some extra addresses
    add_ipv4_defaults();

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "family", "ipv4");

    status = amxb_call(bus_ctx, DEV_PATH, "getAddress", &args, &ret, 5);
    assert_int_equal(status, amxd_status_ok);
    handle_events();

    amxc_var_dump(&ret, 0);
    instance = amxc_var_get_first(GETI_ARG(&ret, 0));
    address = amxc_var_get_key(instance, "Address", AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(amxc_var_constcast(cstring_t, address), "192.168.1.5");

    instance = amxc_var_get_last(GETI_ARG(&ret, 0));
    address = amxc_var_get_key(instance, "Address", AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(amxc_var_constcast(cstring_t, address), "192.168.1.7");

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_getAddress_ipv6(AMXB_UNUSED void** state) {
    int status = 0;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* instance = NULL;
    amxc_var_t* address = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    // add some extra addresses
    add_ipv6_defaults();

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "family", "ipv6");

    status = amxb_call(bus_ctx, DEV_PATH, "getAddress", &args, &ret, 5);
    assert_int_equal(status, amxd_status_ok);
    handle_events();

    amxc_var_dump(&ret, 0);
    instance = amxc_var_get_first(GETI_ARG(&ret, 0));
    address = amxc_var_get_key(instance, "Address", AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(amxc_var_constcast(cstring_t, address), "fe80::f606:8dff:fe11:a5e9");

    instance = amxc_var_get_last(GETI_ARG(&ret, 0));
    address = amxc_var_get_key(instance, "Address", AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(amxc_var_constcast(cstring_t, address), "fc00:1002:fc01:3000:2000::1002");

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_getAddress_both(AMXB_UNUSED void** state) {
    int status = 0;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* instance = NULL;
    amxc_var_t* address = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    add_ipv4_defaults();
    add_ipv6_defaults();

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "family", "");

    status = amxb_call(bus_ctx, DEV_PATH, "getAddress", &args, &ret, 5);
    assert_int_equal(status, amxd_status_ok);
    handle_events();

    amxc_var_dump(&ret, 0);
    instance = amxc_var_get_first(GETI_ARG(&ret, 0));
    address = amxc_var_get_key(instance, "Address", AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(amxc_var_constcast(cstring_t, address), "192.168.1.5");

    instance = amxc_var_get_last(GETI_ARG(&ret, 0));
    address = amxc_var_get_key(instance, "Address", AMXC_VAR_FLAG_DEFAULT);
    assert_string_equal(amxc_var_constcast(cstring_t, address), "fc00:1002:fc01:3000:2000::1002");

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_getAddress_wrong_argument(AMXB_UNUSED void** state) {
    int status = 0;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "family", "ipv5");

    status = amxb_call(bus_ctx, DEV_PATH, "getAddress", &args, &ret, 5);
    assert_int_equal(status, amxd_status_invalid_value);
    handle_events();

    amxc_var_dump(&ret, 0);
    assert_null(amxc_var_get_first(GETI_ARG(&ret, 0)));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_deleteAddressInstance_ipv4(AMXB_UNUSED void** state) {
    int status = 0;
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* template = amxd_object_get_child(dev_obj, MIB_IPv4);
    amxc_var_init(&args);
    amxc_var_init(&ret);

    add_ipv4_defaults();

    assert_non_null(amxd_object_get_instance(template, NULL, 1));

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "address", "192.168.1.5");

    status = amxb_call(bus_ctx, DEV_IPv4_PATH, "deleteAddressInstance", &args, &ret, 5);
    assert_int_equal(status, amxd_status_ok);
    handle_events();

    assert_null(amxd_object_get_instance(template, NULL, 1));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_deleteAddressInstance_ipv6(AMXB_UNUSED void** state) {
    int status = 0;
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* template = amxd_object_get_child(dev_obj, MIB_IPv6);
    amxc_var_init(&args);
    amxc_var_init(&ret);

    add_ipv6_defaults();

    assert_non_null(amxd_object_get_instance(template, NULL, 2));

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "address", "fc00:1002:fc01:3000:2000::1001");

    status = amxb_call(bus_ctx, DEV_IPv6_PATH, "deleteAddressInstance", &args, &ret, 5);
    assert_int_equal(status, amxd_status_ok);
    handle_events();

    assert_null(amxd_object_get_instance(template, NULL, 2));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_delete_last_ipv4_instance(AMXB_UNUSED void** state) {
    /*
     * The instance with index 2 is the last DHCP instance.
     * Therefore it should not be possible to delete it
     */
    int status = 0;
    char* value = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* template = amxd_object_get_child(dev_obj, MIB_IPv4);
    amxd_object_t* instance = NULL;
    amxc_var_init(&args);
    amxc_var_init(&ret);

    add_ipv4_defaults();

    assert_non_null(amxd_object_get_instance(template, NULL, 3));

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "address", "192.168.1.7");

    status = amxb_call(bus_ctx, DEV_IPv4_PATH, "deleteAddressInstance", &args, &ret, 5);
    assert_int_equal(status, amxd_status_ok);
    assert_string_equal(amxc_var_constcast(cstring_t, amxc_var_get_first(&ret)),
                        "Deletion is blocked as this is the last instance of its type");
    handle_events();

    instance = amxd_object_get_instance(template, NULL, 3);
    assert_non_null(instance);
    value = amxd_object_get_value(cstring_t, instance, "Status", NULL);
    assert_string_equal(value, "not reachable");

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    free(value);
}

void test_delete_last_ipv6_instance(AMXB_UNUSED void** state) {
    /*
     * The instance with index 2 is the last LLA instance.
     * Therefore it should not be possible to delete it
     */
    int status = 0;
    char* value = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* template = amxd_object_get_child(dev_obj, MIB_IPv6);
    amxd_object_t* instance = NULL;
    amxc_var_init(&args);
    amxc_var_init(&ret);

    add_ipv6_defaults();

    assert_non_null(amxd_object_get_instance(template, NULL, 1));

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "address", "fe80::f606:8dff:fe11:a5e9");

    status = amxb_call(bus_ctx, DEV_IPv6_PATH, "deleteAddressInstance", &args, &ret, 5);
    assert_int_equal(status, amxd_status_ok);
    assert_string_equal(amxc_var_constcast(cstring_t, amxc_var_get_first(&ret)),
                        "Deletion is blocked as this is the last instance of its type");
    handle_events();

    instance = amxd_object_get_instance(template, NULL, 1);
    assert_non_null(instance);
    value = amxd_object_get_value(cstring_t, instance, "Status", NULL);
    assert_string_equal(value, "not reachable");

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    free(value);
}

void test_remove_not_reachable_when_new_instance_added_ipv4(AMXB_UNUSED void** state) {
    amxc_var_t ret;
    amxd_object_t* template = amxd_object_get_child(dev_obj, MIB_IPv4);
    amxp_expr_t expr;

    amxc_var_init(&ret);

    add_ipv4_defaults();

    /* Add extra 'not reachable' static device, as only the dhcp devices should be removed*/
    add_ip_instance(dev_obj, "192.168.1.8", "Static", "not reachable", MIB_IPv4);

    amxp_expr_buildf(&expr, "AddressSource == 'DHCP' && Status == 'not reachable'");
    assert_non_null(amxd_object_find_instance(template, &expr));
    assert_non_null(amxd_object_get_instance(template, NULL, 1));
    assert_non_null(amxd_object_get_instance(template, NULL, 2));
    assert_non_null(amxd_object_get_instance(template, NULL, 3));
    assert_non_null(amxd_object_get_instance(template, NULL, 4));
    assert_null(amxd_object_get_instance(template, NULL, 5));

    expect_string_count(__wrap_gmap_device_setActive, key, "temp", 1);
    expect_value_count(__wrap_gmap_device_setActive, active, true, 1);
    expect_string_count(__wrap_gmap_device_setActive, source, "mib-ip", 1);
    expect_value_count(__wrap_gmap_device_setActive, priority, 50, 1);

    will_return(__wrap_gmap_device_setActive, 0);
    add_ip_instance(dev_obj, "192.168.1.9", "DHCP", "reachable", MIB_IPv4);

    /* Check that only the 'not reachable' instances are removed
     * It should also only remove the 'Static' instances.
     */
    amxp_expr_clean(&expr);
    amxp_expr_buildf(&expr, "AddressSource == 'DHCP' && Status == 'not reachable'");
    assert_null(amxd_object_find_instance(template, &expr));
    assert_null(amxd_object_get_instance(template, NULL, 2));
    assert_non_null(amxd_object_get_instance(template, NULL, 1));
    assert_non_null(amxd_object_get_instance(template, NULL, 3));
    assert_non_null(amxd_object_get_instance(template, NULL, 4));
    assert_non_null(amxd_object_get_instance(template, NULL, 5));

    amxc_var_clean(&ret);
    amxp_expr_clean(&expr);
}

void test_remove_not_reachable_when_new_instance_added_ipv6(AMXB_UNUSED void** state) {
    amxc_var_t ret;
    amxd_object_t* template = amxd_object_get_child(dev_obj, MIB_IPv6);

    amxc_var_init(&ret);

    add_ipv6_defaults();

    /* Add extra 'not reachable' GUA device, as only the ULA devices should be removed*/
    add_ip_instance(dev_obj, "2a02:1802:94:4281:4a8d:36ff:fe5d:b0ee", "Static", "not reachable", MIB_IPv6);

    assert_non_null(amxd_object_get_instance(template, NULL, 1));
    assert_non_null(amxd_object_get_instance(template, NULL, 2));
    assert_non_null(amxd_object_get_instance(template, NULL, 3));
    assert_non_null(amxd_object_get_instance(template, NULL, 4));

    expect_string_count(__wrap_gmap_device_setActive, key, "temp", 1);
    expect_value_count(__wrap_gmap_device_setActive, active, true, 1);
    expect_string_count(__wrap_gmap_device_setActive, source, "mib-ip", 1);
    expect_value_count(__wrap_gmap_device_setActive, priority, 50, 1);

    will_return(__wrap_gmap_device_setActive, 0);
    add_ip_instance(dev_obj, "fc00:1002:fc01:3000:2000::1003", "Static", "reachable", MIB_IPv6);

    /* Check that only the 'not reachable' instances are removed
     * It should also only remove the ULA instances (fc00:...)
     */
    assert_null(amxd_object_get_instance(template, NULL, 3));
    assert_non_null(amxd_object_get_instance(template, NULL, 1));
    assert_non_null(amxd_object_get_instance(template, NULL, 2));
    assert_non_null(amxd_object_get_instance(template, NULL, 4));
    assert_non_null(amxd_object_get_instance(template, NULL, 5));

    amxc_var_clean(&ret);
}

void test_remove_last_instance_not_lan_device(AMXB_UNUSED void** state) {
    int status = 0;
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* template = amxd_object_get_child(dev_lan, MIB_IPv4);
    amxd_object_t* instance = NULL;
    amxc_var_init(&args);
    amxc_var_init(&ret);

    assert_non_null(dev_lan);
    assert_non_null(template);

    expect_string_count(__wrap_gmap_device_setActive, key, "lan", 1);
    expect_value_count(__wrap_gmap_device_setActive, active, true, 1);
    expect_string_count(__wrap_gmap_device_setActive, source, "mib-ip", 1);
    expect_value_count(__wrap_gmap_device_setActive, priority, 50, 1);

    will_return(__wrap_gmap_device_setActive, 1);
    add_ip_instance(dev_lan, "192.168.1.5", "DHCP", "reachable", MIB_IPv4);

    instance = amxd_object_get_instance(template, NULL, 1);
    assert_non_null(instance);

    expect_string_count(__wrap_gmap_device_setActive, key, "lan", 1);
    expect_value_count(__wrap_gmap_device_setActive, active, false, 1);
    expect_string_count(__wrap_gmap_device_setActive, source, "mib-ip", 1);
    expect_value_count(__wrap_gmap_device_setActive, priority, 50, 1);
    will_return(__wrap_gmap_device_setActive, 1);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "address", "192.168.1.5");

    status = amxb_call(bus_ctx, "Devices.Device.lan.IPv4Address.", "deleteAddressInstance", &args, &ret, 5);
    assert_int_equal(status, amxd_status_ok);
    assert_string_equal(amxc_var_constcast(cstring_t, amxc_var_get_first(&ret)),
                        "OK");
    handle_events();

    instance = amxd_object_get_instance(template, NULL, 3);
    assert_null(instance);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

int32_t __wrap_gmap_device_setActive(const char* key,
                                     bool active,
                                     const char* source,
                                     uint32_t priority) {
    check_expected(key);
    check_expected(active);
    check_expected(source);
    check_expected(priority);

    return (int) mock();
}
