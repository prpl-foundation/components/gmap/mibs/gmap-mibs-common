/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_information.h"
#include "../common/dummy.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_variant.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_expression.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <amxo/amxo_mibs.h>

#include <gmap/gmap.h>

amxd_status_t _setInformation(amxd_object_t* object,
                              amxd_function_t* func,
                              amxc_var_t* args,
                              amxc_var_t* ret);

amxd_status_t _setInformationList(amxd_object_t* object,
                                  amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret);

amxd_status_t _removeInformation(amxd_object_t* object,
                                 amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret);

/**********************************************************
* Macro definitions
**********************************************************/

#define MIB_INFORMATION_MODEL_NAME "ModelName"
#define MIB_INFORMATION_MODEL_NAMES "ModelNames"
#define MIB_INFORMATION_MODEL_NAME_DEFAULT ""

#define MIB_INFORMATION_DEVICE_CATEGORY "DeviceCategory"
#define MIB_INFORMATION_DEVICE_CATEGORIES "DeviceCategories"
#define MIB_INFORMATION_DEVICE_CATEGORY_DEFAULT "unknown"

/**********************************************************
* Variable declarations
**********************************************************/
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxd_object_t* dev_obj = NULL;

static bool load_mib_module() {
    amxo_parser_t* parser = test_get_parser();
    amxd_dm_t* dm = test_get_dm();
    dev_obj = amxd_object_findf(&dm->object, "Devices.Device.temp.");

    assert_int_equal(amxo_resolver_ftab_add(parser, "setInformation", AMXO_FUNC(_setInformation)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "setInformationList", AMXO_FUNC(_setInformationList)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "removeInformation", AMXO_FUNC(_removeInformation)), 0);

    amxo_parser_scan_mib_dir(parser, "../../mibs");
    amxo_parser_load_mib(parser, dm, "information");

    amxo_parser_apply_mib(parser, dev_obj, "information");

    return true;
}

int test_information_setup(AMXB_UNUSED void** state) {
    amxc_var_t* var = NULL;
    amxo_parser_t* parser = NULL;

    assert_int_equal(test_register_dummy(), 0);
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    gmap_client_init(bus_ctx);

    // parser and dm are created in dummy connect
    parser = test_get_parser();

    assert_int_equal(amxo_resolver_ftab_add(parser, "load", AMXO_FUNC(_load)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "get", AMXO_FUNC(_get)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "setTag", AMXO_FUNC(_setTag)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "clearTag", AMXO_FUNC(_clearTag)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "setActive", AMXO_FUNC(_setActive)), 0);

    gmap_client_set_mib_server_dm(test_get_dm());

    // do not load import so files
    var = amxo_parser_claim_config(parser, "odl-import");
    amxc_var_set(bool, var, false);

    assert_int_equal(test_load_dummy_remote("../common/dummy_gmap.odl"), 0);

    load_mib_module();

    return amxd_status_ok;
}

int test_information_teardown(AMXB_UNUSED void** state) {
    amxo_parser_remove_mibs(test_get_parser(), dev_obj, NULL);
    amxb_disconnect(bus_ctx);
    amxb_free(&bus_ctx);
    assert_int_equal(test_unregister_dummy(), 0);

    return amxd_status_ok;
}

void test_set_information(AMXB_UNUSED void** state) {
    char* source = "test_model";
    char* value = "sah";
    char* return_value = NULL;
    uint32_t priority = 4;
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_INFORMATION_MODEL_NAMES);

    assert_non_null(type_collection);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", MIB_INFORMATION_MODEL_NAME);
    amxc_var_add_key(cstring_t, &args, "source", source);
    amxc_var_add_key(cstring_t, &args, "value", value);
    amxc_var_add_key(uint32_t, &args, "priority", priority);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setInformation",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    return_value = amxd_object_get_value(cstring_t, dev_obj, MIB_INFORMATION_MODEL_NAME, NULL);
    assert_string_equal(return_value, value);
    free(return_value);

    amxp_expr_buildf(&expr, "Source == '%s'", source);
    assert_non_null(amxd_object_find_instance(type_collection, &expr));

    amxp_expr_clean(&expr);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_update_information(AMXB_UNUSED void** state) {
    char* source = "test_model";
    char* value = "sah2";
    char* return_value = NULL;
    uint32_t priority = 4;
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_INFORMATION_MODEL_NAMES);

    assert_non_null(type_collection);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", MIB_INFORMATION_MODEL_NAME);
    amxc_var_add_key(cstring_t, &args, "source", source);
    amxc_var_add_key(cstring_t, &args, "value", value);
    amxc_var_add_key(uint32_t, &args, "priority", priority);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setInformation",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    return_value = amxd_object_get_value(cstring_t, dev_obj, MIB_INFORMATION_MODEL_NAME, NULL);
    assert_string_equal(return_value, value);
    free(return_value);

    amxp_expr_buildf(&expr, "Source == '%s' and Priority == %d", source, priority);
    assert_non_null(amxd_object_find_instance(type_collection, &expr));

    amxp_expr_clean(&expr);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_determine_priority(AMXB_UNUSED void** state) {
    char* source = "test_model_higher_prio";
    char* value = "other_sah";
    char* return_value = NULL;
    uint32_t priority = 3;
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_INFORMATION_MODEL_NAMES);

    assert_non_null(type_collection);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", MIB_INFORMATION_MODEL_NAME);
    amxc_var_add_key(cstring_t, &args, "source", source);
    amxc_var_add_key(cstring_t, &args, "value", value);
    amxc_var_add_key(uint32_t, &args, "priority", priority);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setInformation",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    return_value = amxd_object_get_value(cstring_t, dev_obj, MIB_INFORMATION_MODEL_NAME, NULL);
    assert_string_equal(return_value, value);
    free(return_value);

    amxp_expr_buildf(&expr, "Source == '%s' and Priority == %d", source, priority);
    assert_non_null(amxd_object_find_instance(type_collection, &expr));

    amxp_expr_clean(&expr);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_determine_priority2(AMXB_UNUSED void** state) {
    char* source = "test_model_higher_prio";
    char* value = "other_sah";
    char* return_value = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_INFORMATION_MODEL_NAMES);

    assert_non_null(type_collection);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", MIB_INFORMATION_MODEL_NAME);
    amxc_var_add_key(cstring_t, &args, "source", source);
    amxc_var_add_key(cstring_t, &args, "value", value);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setInformation",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    return_value = amxd_object_get_value(cstring_t, dev_obj, MIB_INFORMATION_MODEL_NAME, NULL);
    assert_string_not_equal(return_value, value);
    free(return_value);

    amxp_expr_buildf(&expr, "Source == '%s' and Priority == %d", source, 100);
    assert_non_null(amxd_object_find_instance(type_collection, &expr));

    amxp_expr_clean(&expr);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_wrong_type(AMXB_UNUSED void** state) {
    char* source = "test_model_higher_prio";
    char* value = "sahsah";
    uint32_t priority = 5;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", "WrongType");
    amxc_var_add_key(cstring_t, &args, "source", source);
    amxc_var_add_key(cstring_t, &args, "value", value);
    amxc_var_add_key(uint32_t, &args, "priority", priority);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setInformation",
                                                 &args,
                                                 &ret),
                     amxd_status_object_not_found);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_wrong_priority(AMXB_UNUSED void** state) {
    char* source = "test_model_higher_prio";
    char* value = "sahsah";
    uint32_t priority = 500;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", "WrongType");
    amxc_var_add_key(cstring_t, &args, "source", source);
    amxc_var_add_key(cstring_t, &args, "value", value);
    amxc_var_add_key(uint32_t, &args, "priority", priority);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setInformation",
                                                 &args,
                                                 &ret),
                     amxd_status_invalid_arg);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_no_type(AMXB_UNUSED void** state) {
    char* source = "test_model_higher_prio";
    char* value = "sahsah";
    uint32_t priority = 5;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", "");
    amxc_var_add_key(cstring_t, &args, "source", source);
    amxc_var_add_key(cstring_t, &args, "value", value);
    amxc_var_add_key(uint32_t, &args, "priority", priority);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setInformation",
                                                 &args,
                                                 &ret),
                     amxd_status_missing_key);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_no_source(AMXB_UNUSED void** state) {
    char* value = "sahsah";
    uint32_t priority = 5;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", "WrongType");
    amxc_var_add_key(cstring_t, &args, "source", "");
    amxc_var_add_key(cstring_t, &args, "value", value);
    amxc_var_add_key(uint32_t, &args, "priority", priority);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setInformation",
                                                 &args,
                                                 &ret),
                     amxd_status_missing_key);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_no_value(AMXB_UNUSED void** state) {
    char* source = "test_model_higher_prio";
    uint32_t priority = 5;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", "WrongType");
    amxc_var_add_key(cstring_t, &args, "source", source);
    amxc_var_add_key(cstring_t, &args, "value", "");
    amxc_var_add_key(uint32_t, &args, "priority", priority);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setInformation",
                                                 &args,
                                                 &ret),
                     amxd_status_missing_key);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_remove_information(AMXB_UNUSED void** state) {
    char* source = "test_model";
    char* return_value = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_INFORMATION_MODEL_NAMES);

    assert_non_null(type_collection);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", MIB_INFORMATION_MODEL_NAME);
    amxc_var_add_key(cstring_t, &args, "source", source);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "removeInformation",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    return_value = amxd_object_get_value(cstring_t, dev_obj, MIB_INFORMATION_MODEL_NAME, NULL);
    assert_string_equal(return_value, "other_sah");
    free(return_value);

    amxp_expr_buildf(&expr, "Source == '%s'", source);
    assert_null(amxd_object_find_instance(type_collection, &expr));

    amxp_expr_clean(&expr);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_no_information_left(AMXB_UNUSED void** state) {
    char* source = "test_model_higher_prio";
    char* return_value = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_INFORMATION_MODEL_NAMES);

    assert_non_null(type_collection);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", MIB_INFORMATION_MODEL_NAME);
    amxc_var_add_key(cstring_t, &args, "source", source);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "removeInformation",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    return_value = amxd_object_get_value(cstring_t, dev_obj, MIB_INFORMATION_MODEL_NAME, NULL);
    assert_string_equal(return_value, MIB_INFORMATION_MODEL_NAME_DEFAULT);
    free(return_value);

    amxp_expr_buildf(&expr, "Source == '%s'", source);
    assert_null(amxd_object_find_instance(type_collection, &expr));

    amxp_expr_clean(&expr);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_add_information_list(AMXB_UNUSED void** state) {
    char* return_value = NULL;
    amxc_var_t args_list;
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_INFORMATION_MODEL_NAMES);

    assert_non_null(type_collection);

    amxc_var_init(&args_list);
    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args_list, AMXC_VAR_ID_LIST);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", MIB_INFORMATION_MODEL_NAME);
    amxc_var_add_key(cstring_t, &args, "source", "source1");
    amxc_var_add_key(cstring_t, &args, "value", "value1");
    amxc_var_add_key(uint32_t, &args, "priority", 1);
    amxc_var_add(amxc_htable_t, &args_list, amxc_var_constcast(amxc_htable_t, &args));

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", MIB_INFORMATION_MODEL_NAME);
    amxc_var_add_key(cstring_t, &args, "source", "source2");
    amxc_var_add_key(cstring_t, &args, "value", "value2");
    amxc_var_add(amxc_htable_t, &args_list, amxc_var_constcast(amxc_htable_t, &args));

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(amxc_llist_t, &args, "informationList", amxc_var_constcast(amxc_llist_t, &args_list));

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setInformationList",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    return_value = amxd_object_get_value(cstring_t, dev_obj, MIB_INFORMATION_MODEL_NAME, NULL);
    assert_string_equal(return_value, "value1");
    free(return_value);

    amxp_expr_buildf(&expr, "Source == 'source1' and Priority == 1");
    assert_non_null(amxd_object_find_instance(type_collection, &expr));
    amxp_expr_clean(&expr);

    amxp_expr_buildf(&expr, "Source == 'source2' and Priority == 100");
    assert_non_null(amxd_object_find_instance(type_collection, &expr));

    amxp_expr_clean(&expr);
    amxc_var_clean(&args_list);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_information_y(AMXB_UNUSED void** state) {
    char* source = "test_model";
    char* value = "sah";
    char* return_value = NULL;
    uint32_t priority = 4;
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_INFORMATION_DEVICE_CATEGORIES);

    assert_non_null(type_collection);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", MIB_INFORMATION_DEVICE_CATEGORY);
    amxc_var_add_key(cstring_t, &args, "source", source);
    amxc_var_add_key(cstring_t, &args, "value", value);
    amxc_var_add_key(uint32_t, &args, "priority", priority);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setInformation",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    return_value = amxd_object_get_value(cstring_t, dev_obj, MIB_INFORMATION_DEVICE_CATEGORY, NULL);
    assert_string_equal(return_value, value);
    free(return_value);

    amxp_expr_buildf(&expr, "Source == '%s'", source);
    assert_non_null(amxd_object_find_instance(type_collection, &expr));

    amxp_expr_clean(&expr);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_update_information_y(AMXB_UNUSED void** state) {
    char* source = "test_model";
    char* value = "sah2";
    char* return_value = NULL;
    uint32_t priority = 4;
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_INFORMATION_DEVICE_CATEGORIES);

    assert_non_null(type_collection);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", MIB_INFORMATION_DEVICE_CATEGORY);
    amxc_var_add_key(cstring_t, &args, "source", source);
    amxc_var_add_key(cstring_t, &args, "value", value);
    amxc_var_add_key(uint32_t, &args, "priority", priority);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setInformation",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    return_value = amxd_object_get_value(cstring_t, dev_obj, MIB_INFORMATION_DEVICE_CATEGORY, NULL);
    assert_string_equal(return_value, value);
    free(return_value);

    amxp_expr_buildf(&expr, "Source == '%s' and Priority == %d", source, priority);
    assert_non_null(amxd_object_find_instance(type_collection, &expr));

    amxp_expr_clean(&expr);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_remove_information_y(AMXB_UNUSED void** state) {
    char* source = "test_model";
    char* return_value = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_INFORMATION_DEVICE_CATEGORIES);

    assert_non_null(type_collection);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", MIB_INFORMATION_DEVICE_CATEGORY);
    amxc_var_add_key(cstring_t, &args, "source", source);

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "removeInformation",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    return_value = amxd_object_get_value(cstring_t, dev_obj, MIB_INFORMATION_DEVICE_CATEGORY, NULL);
    assert_string_equal(return_value, MIB_INFORMATION_DEVICE_CATEGORY_DEFAULT);
    free(return_value);

    amxp_expr_buildf(&expr, "Source == '%s'", source);
    assert_null(amxd_object_find_instance(type_collection, &expr));

    amxp_expr_clean(&expr);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_add_information_list_y(AMXB_UNUSED void** state) {
    char* return_value = NULL;
    amxc_var_t args_list;
    amxc_var_t args;
    amxc_var_t ret;
    amxp_expr_t expr;
    amxd_object_t* type_collection = amxd_object_get_child(dev_obj, MIB_INFORMATION_DEVICE_CATEGORIES);

    assert_non_null(type_collection);

    amxc_var_init(&args_list);
    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args_list, AMXC_VAR_ID_LIST);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", MIB_INFORMATION_DEVICE_CATEGORY);
    amxc_var_add_key(cstring_t, &args, "source", "source1");
    amxc_var_add_key(cstring_t, &args, "value", "value1");
    amxc_var_add_key(uint32_t, &args, "priority", 1);
    amxc_var_add(amxc_htable_t, &args_list, amxc_var_constcast(amxc_htable_t, &args));

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", MIB_INFORMATION_DEVICE_CATEGORY);
    amxc_var_add_key(cstring_t, &args, "source", "source2");
    amxc_var_add_key(cstring_t, &args, "value", "value2");
    amxc_var_add(amxc_htable_t, &args_list, amxc_var_constcast(amxc_htable_t, &args));

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(amxc_llist_t, &args, "informationList", amxc_var_constcast(amxc_llist_t, &args_list));

    assert_int_equal(amxd_object_invoke_function(dev_obj,
                                                 "setInformationList",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    return_value = amxd_object_get_value(cstring_t, dev_obj, MIB_INFORMATION_DEVICE_CATEGORY, NULL);
    assert_string_equal(return_value, "value1");
    free(return_value);

    amxp_expr_buildf(&expr, "Source == 'source1' and Priority == 1");
    assert_non_null(amxd_object_find_instance(type_collection, &expr));
    amxp_expr_clean(&expr);

    amxp_expr_buildf(&expr, "Source == 'source2' and Priority == 100");
    assert_non_null(amxd_object_find_instance(type_collection, &expr));

    amxp_expr_clean(&expr);
    amxc_var_clean(&args_list);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
