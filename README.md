# gMap common mibs

## Introduction
Common mib files for the amx gmap port. These mib files provide additions to the gMap data model common for several gmap modules.

## Installing

#### Using make target install

```bash
make install
```
